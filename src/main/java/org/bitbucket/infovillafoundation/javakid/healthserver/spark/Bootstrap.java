package org.bitbucket.infovillafoundation.javakid.healthserver.spark;

import org.bitbucket.infovillafoundation.javakid.healthserver.spark.doctor.*;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient.DeletePatientRoute;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient.GetPatientsRoute;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient.GetPatientRoute;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient.PutPatientRoute;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient.PostPatientRoute;
import spark.Spark;

public class Bootstrap {

    public static void main(String[] args) {
        Spark.setPort(8085);

        Spark.get(new GetPatientsRoute("/patients"));
        Spark.get(new GetPatientRoute("/patients/:id"));
        Spark.post(new PostPatientRoute("/patients"));
        Spark.put(new PutPatientRoute("/patients/:id"));
        Spark.delete(new DeletePatientRoute("/patients/:id"));

        Spark.get(new GetDoctorsRoute("/doctors"));
        Spark.get(new GetDoctorRoute("/doctors/:id"));
        Spark.post(new PostDoctorRoute("/doctors"));
        Spark.put(new PutDoctorRoute("/doctors/:id"));
        Spark.delete(new DeleteDoctorRoute("/doctors/:id"));
    }
}
