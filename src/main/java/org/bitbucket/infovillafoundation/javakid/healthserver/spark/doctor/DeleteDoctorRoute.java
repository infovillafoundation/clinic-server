package org.bitbucket.infovillafoundation.javakid.healthserver.spark.doctor;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Doctor;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

public class DeleteDoctorRoute extends JsonTransformer {

    public DeleteDoctorRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        long doctorId = Long.parseLong(request.params(":id"));
        Doctor doctor = Ebean.find(Doctor.class, doctorId);
        if (doctor != null) {
            Ebean.delete(doctor);
            response.status(204); // 204 No Content
            return "";
        } else {
            response.status(404); // 404 Not found
            return createErrorResponse("Not found");
        }
    }

}
