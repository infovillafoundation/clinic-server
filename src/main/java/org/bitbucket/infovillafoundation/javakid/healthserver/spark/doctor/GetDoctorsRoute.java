package org.bitbucket.infovillafoundation.javakid.healthserver.spark.doctor;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Doctor;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

public class GetDoctorsRoute extends JsonTransformer {

    public GetDoctorsRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        System.out.println(Ebean.find(Doctor.class).findList().size());
        return Ebean.find(Doctor.class).findList();
    }
    
}
