package org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

public class GetPatientsRoute extends JsonTransformer {

    public GetPatientsRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        System.out.println(Ebean.find(Patient.class).findList().size());
        return Ebean.find(Patient.class).findList();
    }
    
}
