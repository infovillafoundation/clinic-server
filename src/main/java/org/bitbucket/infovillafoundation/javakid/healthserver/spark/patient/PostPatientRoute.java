package org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient;

import java.io.IOException;

import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

import com.avaje.ebean.Ebean;

public class PostPatientRoute extends JsonTransformer {
    public PostPatientRoute(String path) {
        super(path);
    }



    @Override
    public Object handle(Request request, Response response) {
        Patient patient = null;
		try {
            patient = mapper.readValue(request.body(), Patient.class);
		} catch (IOException e) {
			
			response.status(500); // Server-side error
			
			return createErrorResponse("Patient couldn't be saved");
		}
        Ebean.save(patient);
        response.status(201); // 201 Created
        return patient;
    }
    
}
