package org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Gender;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PutPatientRoute extends JsonTransformer {

    public PutPatientRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        long patientId = Long.parseLong(request.params(":id"));
        Patient patient = Ebean.find(Patient.class, patientId);
        if (patient != null) {
            String newName = request.queryParams("name");
            if (newName != null) {
                patient.setName(newName);
            }
            String newNirc = request.queryParams("nirc");
            if (newNirc != null) {
                patient.setNirc(newNirc);
            }
            String newGender = request.queryParams("gender");
            if (newGender != null) {
                if (newGender.equals("male")) {
                    patient.setGender(Gender.Male);
                }
                else {
                    patient.setGender(Gender.Female);
                }
            }
            String newEmail = request.queryParams("email");
            if (newEmail != null) {
                patient.setEmail(newEmail);
            }
            String newDob = request.queryParams("dob");
            System.out.println("dob: " + newDob);
            if (newDob != null) {
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date newDobUpdate = null;
                try {
                    newDobUpdate = format.parse(newDob);
                } catch (ParseException e) {

                }
                System.out.println("dobas date: " + newDobUpdate);
                patient.setDateOfBirth(newDobUpdate);
            }
            String newPhoneNumber = request.queryParams("phonenum");
            if (newPhoneNumber != null) {
                patient.setPhoneNumber(newPhoneNumber);
            }

            Ebean.update(patient);
            response.status(204); // 204 No Content
            return "";
        } else {
            response.status(404); // 404 Not found
            return createErrorResponse("Not found");
        }
    }
    
}
